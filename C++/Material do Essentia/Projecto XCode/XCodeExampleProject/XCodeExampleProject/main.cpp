//
//  main.cpp
//  XCodeExampleProject
//
//  Created by Nadine Kroher on 14/03/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#include <iostream>

#include "essentia.h"
#include "taglib.h"
#include "fftw3.h"
#include <essentia/algorithmfactory.h>
#include <essentia/essentiamath.h>
#include <essentia/pool.h>
#include <essentia/scheduler/network.h>
#include <essentia/streaming/algorithms/poolstorage.h>

#define STATS_FILE "Spectral Peaks Stats.json"

using namespace std;
using namespace essentia::streaming;
using namespace essentia::scheduler;


int main(int argc, const char * argv[]) {
    // insert code here...
    //std::cout << "Hello, World!\n";
    
    if (argc != 3) {
        cout << "INPUT = " << argv[1] << endl;
        cout << "OUTPUT = " << argv[2] << endl;
        
        cout << "ERROR: incorrect number of arguments." << endl;
        cout << "Usage: " << argv[0] << " audio_input output" << endl;
        exit(1);
    }
    
    string audioFilename = argv[1];
    string outputFilename = argv[2];
    
    // To register the algorithms in the factory (ies)
    essentia::init();
    
    essentia::Pool pool;
    
    /////// PARAMS //////////////
    int frameSize = 2048;
    int hopSize = 1024;
    essentia::Real sampleRate = 44100.0;
    
    //To accomplish the Speactral peaks, one needs to do the following algorithms:
    // audioloader (in case of stereo sound) and Monoloader in case of an mono output  -> framecutter -> Windowing -> Spectrum -> Spectral Peaks
    
    AlgorithmFactory& factory = essentia::streaming::AlgorithmFactory::instance();
    
    Algorithm * algAudio = factory.create("MonoLoader", "filename", audioFilename,"sampleRate", sampleRate);
    
    Algorithm * algFrameCutter = factory.create("FrameCutter","frameSize", frameSize,"hopSize", hopSize);
    Algorithm * algWindowing = factory.create("Windowing", "type", "hamming");
    Algorithm* algSpectrum  = factory.create("Spectrum");
    
    Algorithm *algSpectralPeaks = factory.create("SpectralPeaks","maxFrequency", 20000);
    
    // This algorithms will be used together, as a pipeline, to apply the changes in the wanted order. The order and the changes chosen earlier will give as the Spectral Peaks. The following code will be encharge of the scheduling of the functions. First the MonoLoader will pass the information to the frameCutter, and so on...
       essentia::shutdown();
    
    
    return 0;
}
