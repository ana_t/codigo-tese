//
//  main.cpp
//  XCodeExampleProject
//
//  Created by Nadine Kroher on 14/03/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#include <iostream>

#include "essentia.h"
#include "taglib.h"
#include "fftw3.h"
#include <essentia/algorithmfactory.h>
#include <essentia/essentiamath.h>
#include <essentia/pool.h>
#include <essentia/scheduler/network.h>
#include <essentia/streaming/algorithms/poolstorage.h>
#include <string>

#define STATS_FILE "Spectral Peaks Stats.json"

using namespace std;
using namespace essentia::streaming;
using namespace essentia::scheduler;

string setOutputFileName(string filepath){
    
    size_t aux_position_of_slash =  filepath.find("/");

    string aux_file_name = filepath;
    string aux_file_name2 = filepath;

    size_t real_pos = aux_position_of_slash ;
    size_t type_pos = aux_position_of_slash ;


    while(aux_position_of_slash != std::string::npos){
        //cout << "Filename Intermédio  = " << aux_file_name << " real_pos = " << real_pos << endl;
        aux_file_name = aux_file_name.substr(aux_position_of_slash+1);
        type_pos = real_pos;
        if(aux_position_of_slash!=0)
            real_pos+=aux_position_of_slash+1;
        else real_pos++;
        aux_position_of_slash =  aux_file_name.find("/");
    }
    aux_file_name=filepath.substr(0, real_pos);// path inteiro tirando nome musica
    string type = aux_file_name.substr(type_pos,real_pos);
    string nameOfSong = filepath.substr(real_pos);
    real_pos =  type.find("/");
    type = type.substr(0,real_pos);
    string basePath  = aux_file_name2.substr(0, type_pos-8);
    cout << "Type = " << type << endl;
    cout << "Name of song = " << nameOfSong << endl;
    cout << "BasePath = " << basePath << endl;
    cout << "Path  = " << aux_file_name  << endl;

    return basePath + "C++/Essentia/Build/Products/Debug/Peaks/" + nameOfSong + "/" + type + "/Plots/Spectrograms/";
}
int main(int argc, const char * argv[]) {
    // insert code here...
    //std::cout << "Hello, World!\n";

    if (argc != 2) {
        cout << "INPUT = " << argv[1] << endl;
        //cout << "OUTPUT = " << argv[2] << endl;
        
        cout << "ERROR: incorrect number of arguments." << endl;
        cout << "Usage: " << argv[0] << " audio_input" << endl;
        exit(1);
    }
    
    string audioFilename = argv[1];
    string outputFilename = setOutputFileName(audioFilename) + "spectrogram.dat";//argv[2];
    
    // To register the algorithms in the factory (ies)
    essentia::init();
    
    essentia::Pool pool;
    
    /////// PARAMS //////////////
    int frameSize = 1024;
    int hopSize = 512;
    essentia::Real sampleRate = 44100.0;
    
    //To accomplish the Speactral peaks, one needs to do the following algorithms:
    // audioloader (in case of stereo sound) and Monoloader in case of an mono output  -> framecutter -> Windowing -> Spectrum -> Spectral Peaks
    
    AlgorithmFactory& factory = essentia::streaming::AlgorithmFactory::instance();
    
    Algorithm * algAudio = factory.create("MonoLoader", "filename", audioFilename,"sampleRate", sampleRate);
    
    Algorithm * algFrameCutter = factory.create("FrameCutter","frameSize", frameSize,"hopSize", hopSize);
    Algorithm * algWindowing = factory.create("Windowing", "type", "hann");
    Algorithm* algSpectrum  = factory.create("Spectrum");
    
    
    
    // This algorithms will be used together, as a pipeline, to apply the changes in the wanted order. The order and the changes chosen earlier will give as the Spectral Peaks. The following code will be encharge of the scheduling of the functions. First the MonoLoader will pass the information to the frameCutter, and so on...
    cout << "-------- connecting the algorithms --------" << endl;
    
    // Sending Audio -> FrameCutter
    algAudio -> output("audio") >> algFrameCutter->input("signal");
    // FrameCutter -> Windowing
    algFrameCutter->output("frame")  >>  algWindowing->input("frame");
    
    //Windowing -> Spectrum
    algWindowing->output("frame") >>  algSpectrum->input("frame");
    // Spectrum -> SpectralPeaks
    algSpectrum->output("spectrum")  >> PC(pool, "Spectrum");
    
    cout << "-------- started the processing of " <<  audioFilename  << " --------" << endl;
    // The network/pipeline creation
    Network pipeline(algAudio);
    
    // start the operations on the pipeline
    pipeline.run();
    
    cout << "-------- Cleaning resources from this iteration--------" << endl;
    
    pipeline.clear();

    ofstream os_reportFile;
    os_reportFile.open (outputFilename);
    if(!os_reportFile.is_open()){
        cout << "There was a problem writting the file... The program will exit now" << endl;
        return 0;
    }
    // aqui é necessário escrever um ficheiro e retirar a info da pool e ir fazendo para cada frequência
    cout << "-------- Writting the output file--------" << endl;

    vector<vector<essentia::Real>> spectrums = pool.value<vector<vector<essentia::Real>>>("Spectrum");
    int frame_number = 0;
    double frequency_range_per_bin = (sampleRate/2)/spectrums.at(0).size();
    //para cada spectrum
    for(vector<vector<essentia::Real>>::iterator spectrumIterator = spectrums.begin(); spectrumIterator != spectrums.end(); spectrumIterator++){
        
        essentia::Real frameTime = frame_number * (hopSize/sampleRate);
        
        
        int bin_number = 0;
        for(vector<essentia::Real>::iterator binIterator = (*spectrumIterator).begin(); binIterator != (*spectrumIterator).end(); binIterator++){
            
                for(int currentFrequency = bin_number*frequency_range_per_bin ; currentFrequency < (bin_number+1)*frequency_range_per_bin-1; currentFrequency +=10 ){
                    //escreve em ficheiro o valor do tempo frequencia e  amplitude
                    os_reportFile << frameTime << "\t" << currentFrequency << "\t" << (*binIterator) <<endl;
                }
            
            bin_number++;
            
        }
        
        frame_number++;
        os_reportFile << endl; //separates the data by time value by adding a linebreak

    }
    
    os_reportFile.close();
    cout << "Everything as schedule. File written in " <<  outputFilename << endl;
    essentia::shutdown();
    
    
    return 0;
}
