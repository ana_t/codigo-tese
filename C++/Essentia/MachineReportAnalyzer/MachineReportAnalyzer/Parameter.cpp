//
//  Parameter.cpp
//  MachineReportAnalyzer
//
//  Created by Ana on 27/04/15.
//  Copyright (c) 2015 Ana. All rights reserved.
//

#include "Parameter.h"


Parameter::Parameter(int type, int value){
    Parameter::type = type;
    myValue = value;
    success = 0;
    counter = 0;

}

int Parameter::getType(){
    return type;
}

int Parameter::getValue(){
    return myValue;
}

void Parameter::add(double success){
    Parameter::success+=success;
    counter++;
    
}

void Parameter::calculateAverage(){
    success/=counter;
}
double Parameter::getAverage(){
    return success;
}

