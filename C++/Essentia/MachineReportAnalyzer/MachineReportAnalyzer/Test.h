//
//  Test.h
//  MachineReportAnalyzer
//
//  Created by Ana on 22/04/15.
//  Copyright (c) 2015 Ana. All rights reserved.
//

#ifndef __MachineReportAnalyzer__Test__
#define __MachineReportAnalyzer__Test__

#include <stdio.h>

#endif /* defined(__MachineReportAnalyzer__Test__) */

class Test{

    public:
        Test(int frameSize, int hopSize, int number_of_peaks_per_frame, int min_frequency_of_peaks, int max_frequency_of_peaks, double survivor_peaks_pc , int number_of_peaks_in_common, int total_number_of_peaks, double approximate_time_of_song, int number_of_frames);
    
        int getFrameSize();
        int getHopSize();
        int getNumberOfPeaksPerFrame();
        int getMinFrequencyOfPeaks();
        int getMaxFrequencyOfPeaks();
        double getSurvivorPeaksPc();
        int getNumberOfPeaksInCommon();
        int getTotalNumberOfPeaks();
        double getApproximateTimeOfSong();
        int getNumberOfFrames();
    
        bool operator<(Test other) const
        {
        return survivor_peaks_pc > other.getSurvivorPeaksPc();
        }

    
    private:
    
        int frameSize;
        int hopSize;
        int number_of_peaks_per_frame;
        int min_frequency_of_peaks;
        int max_frequency_of_peaks;
        double survivor_peaks_pc;
        int number_of_peaks_in_common;
        int total_number_of_peaks;
        double approximate_time_of_song;
        int number_of_frames;
    

};