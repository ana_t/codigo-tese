//
//  main.cpp
//  MachineReportAnalyzer
//
//  Created by Ana on 22/04/15.
//  Copyright (c) 2015 Ana. All rights reserved.
//
#include <iostream>
#include <fstream>
#include <vector>
#include <set>

#include "Test.h"
//#include "Parameter.h"
#include "MachineReportAnalyzer.h"
using namespace std;

enum parameter_t {MIN_FREQ = 0, MAX_FREQ = 1, NUMBER_OF_PEAKS = 2};




int main(int argc, const char * argv[]) {
    
    string machineReportsPath = argv[1];
    
    // ** read all files from a the machine reports' directory
    
    FILE *pipe;
    int status;
    char path[PATH_MAX];
    vector<string> _sMachineReportsPaths;
    
    string ls = "ls " + machineReportsPath;
    pipe = popen(ls.c_str() , "r");
    
    if (pipe == NULL)
        cout << "Error on executing ls!"<< endl; /* Handle error */
    
    else
    {
        while (fgets(path, PATH_MAX, pipe) != NULL)
        {
            string aux = path;
            aux = aux.substr(0,aux.size()-1);
            if(aux.compare("Performance.report") !=0){
            
            _sMachineReportsPaths.push_back(machineReportsPath + aux);
            }
        }
    }
    status = pclose(pipe);
    if (status == -1) {
        cout << "Error on closing the pipe to shell!"<< endl; /* Handle error */;
    }

    // finished the search for every report in the directory **
    
    
    //*******Creates parameter objects and the datatypes to hold them******
    
    set<Test> allTestsByBestResult;
    
    vector<Parameter> _vMinFreqParameters;
    vector<Parameter> _vMaxFreqParameters;
    vector<Parameter> _vNumberOfPeaksParameters;
    
    Parameter minFreq0 (MIN_FREQ,0);
    Parameter minFreq5000 (MIN_FREQ,5000);
    Parameter minFreq7500 (MIN_FREQ,7500);
    Parameter minFreq2500 (MIN_FREQ,2500);
    Parameter minFreq10000 (MIN_FREQ,10000);
    Parameter minFreq12500 (MIN_FREQ,12500);
    Parameter minFreq15000 (MIN_FREQ,15000);
    
    _vMinFreqParameters.push_back(minFreq0);
    _vMinFreqParameters.push_back(minFreq2500);
    _vMinFreqParameters.push_back(minFreq5000);
    _vMinFreqParameters.push_back(minFreq7500);
    _vMinFreqParameters.push_back(minFreq10000);
    _vMinFreqParameters.push_back(minFreq12500);
    _vMinFreqParameters.push_back(minFreq15000);

    
    Parameter maxFreq15000 (MAX_FREQ,15000);
    Parameter maxFreq17500 (MAX_FREQ,17500);
    Parameter maxFreq20000 (MAX_FREQ,20000);
    Parameter maxFreq22500 (MAX_FREQ,22500);
    Parameter maxFreq25000 (MAX_FREQ,25000);
    
    _vMaxFreqParameters.push_back(maxFreq15000);
    _vMaxFreqParameters.push_back(maxFreq17500);
    _vMaxFreqParameters.push_back(maxFreq20000);
    _vMaxFreqParameters.push_back(maxFreq22500);
    _vMaxFreqParameters.push_back(maxFreq25000);

    
    Parameter number_of_peaks1 (NUMBER_OF_PEAKS,1);
    Parameter number_of_peaks2 (NUMBER_OF_PEAKS,2);
    Parameter number_of_peaks10 (NUMBER_OF_PEAKS,10);
    Parameter number_of_peaks3 (NUMBER_OF_PEAKS,3);
    Parameter number_of_peaks4 (NUMBER_OF_PEAKS,4);
    Parameter number_of_peaks5 (NUMBER_OF_PEAKS,5);
    Parameter number_of_peaks6 (NUMBER_OF_PEAKS,6);
    Parameter number_of_peaks7 (NUMBER_OF_PEAKS,7);
    Parameter number_of_peaks8 (NUMBER_OF_PEAKS,8);
    Parameter number_of_peaks9 (NUMBER_OF_PEAKS,9);
    
    _vNumberOfPeaksParameters.push_back(number_of_peaks1);
    _vNumberOfPeaksParameters.push_back(number_of_peaks10); //on purpose because ls returns by lexical order.
    _vNumberOfPeaksParameters.push_back(number_of_peaks2);
    _vNumberOfPeaksParameters.push_back(number_of_peaks3);
    _vNumberOfPeaksParameters.push_back(number_of_peaks4);
    _vNumberOfPeaksParameters.push_back(number_of_peaks5);
    _vNumberOfPeaksParameters.push_back(number_of_peaks6);
    _vNumberOfPeaksParameters.push_back(number_of_peaks7);
    _vNumberOfPeaksParameters.push_back(number_of_peaks8);
    _vNumberOfPeaksParameters.push_back(number_of_peaks9);
    
    Parameter* minFreqPointer = &_vMinFreqParameters.front();
    Parameter* maxFreqPointer = &_vMaxFreqParameters.front();
    Parameter* numberOfPeaksPointer = &_vNumberOfPeaksParameters.front();



    //****************
    
    
    //** reads every report collected in the previous stage and saves them in a vector
    int counter = 0;
    for(vector<string>::iterator reportIterator = _sMachineReportsPaths.begin(); reportIterator < _sMachineReportsPaths.end(); reportIterator ++){
    
        //cout << "path to ifstream = " + *reportIterator << endl;
    ifstream machineReportFile (*reportIterator);

    int frameSize;
    int hopSize;
    int number_of_peaks_per_frame;
    int min_frequency_of_peaks;
    int max_frequency_of_peaks;
    double survivor_peaks_pc;
    int number_of_peaks_in_common;
    int total_number_of_peaks;
    double approximate_time_of_song;
    int number_of_frames;
    string line;
    if (machineReportFile.is_open())
    {
        getline (machineReportFile,line);
        frameSize = stoi(line);
        getline (machineReportFile,line);
        hopSize = stoi(line);
        getline (machineReportFile,line);
        number_of_peaks_per_frame = stoi(line);
        getline (machineReportFile,line);
        min_frequency_of_peaks = stoi(line);
        getline (machineReportFile,line);
        max_frequency_of_peaks = stoi(line);
        getline (machineReportFile,line);
         survivor_peaks_pc = stod(line);
        getline (machineReportFile,line);
        number_of_peaks_in_common = stoi(line);
        getline (machineReportFile,line);
        total_number_of_peaks = stoi(line);
        getline (machineReportFile,line);
        approximate_time_of_song = stod(line);
        getline (machineReportFile,line);
        number_of_frames = stoi(line);
        
        
        machineReportFile.close();
        Test test = Test( frameSize,  hopSize,  number_of_peaks_per_frame,  min_frequency_of_peaks,  max_frequency_of_peaks,  survivor_peaks_pc ,  number_of_peaks_in_common,  total_number_of_peaks,  approximate_time_of_song,  number_of_frames);

        
        //cout << "FrameSize = " << frameSize <<  " ; HopSize = " <<hopSize <<  " ; Number of peaks = " <<number_of_peaks_per_frame << " ; Min_Freq = " <<  min_frequency_of_peaks <<  " ; Max_Freq = " <<max_frequency_of_peaks <<  "; % Survivor Peaks = " <<survivor_peaks_pc <<  " ; Number of Peaks in common  = " << number_of_peaks_in_common <<  " ; Total number of peaks = " <<total_number_of_peaks <<  " ; Approximated time of song (s) = " <<approximate_time_of_song << " ; Number of frames = " << number_of_frames << endl;
        allTestsByBestResult.insert(test);
        minFreqPointer->add(survivor_peaks_pc);
        maxFreqPointer->add(survivor_peaks_pc);
        numberOfPeaksPointer->add(survivor_peaks_pc);
        if(maxFreqPointer == &_vMaxFreqParameters.back())
            maxFreqPointer = &_vMaxFreqParameters.front();
        else maxFreqPointer +=1;
        
        if(counter%5 == 0 && counter!=0) {
            
            minFreqPointer->calculateAverage();
            if(minFreqPointer == &_vMinFreqParameters.back())
                minFreqPointer = &_vMinFreqParameters.front();
            else minFreqPointer +=1;
            
            //cout << "Mudei MinFreqPointer para " << minFreqPointer->getValue() <<  " com counter="<< counter <<endl;
 
        }
        if(counter%34 == 0 && counter!=0) {
            
            numberOfPeaksPointer->calculateAverage();
            if(numberOfPeaksPointer == &_vNumberOfPeaksParameters.back())
                numberOfPeaksPointer = &_vNumberOfPeaksParameters.front();
            else numberOfPeaksPointer +=1;
            
            //cout << "Mudei NumberOfPeaksPointer para " << numberOfPeaksPointer->getValue() <<  " com counter="<< counter <<endl;
            
        }
            counter++;
    }
    
    else{ cout << "Unable to open file" << endl;
        cerr << "Error code: " << strerror(errno) << endl; // Get some info as to why

    }
    
    
    }
    //calculate Average in max frequency Parameters
    for (vector<Parameter>::iterator maxFrequencyParameters = _vMaxFreqParameters.begin() ; maxFrequencyParameters < _vMaxFreqParameters.end(); maxFrequencyParameters ++ ){
        (*maxFrequencyParameters).calculateAverage();
    }
    
    for (set<Test>::iterator testIterator = allTestsByBestResult.begin() ; testIterator != allTestsByBestResult.end(); testIterator ++ ){
        Test t1 =(*testIterator);
        cout <<  t1.getSurvivorPeaksPc() <<"%"  <<endl;
    }
    
    set<Parameter> orderedMinFreqBestResults= MachineReportAnalyzer::calculateBestValueForParameter(_vMinFreqParameters);
    Parameter pMinFreq = *orderedMinFreqBestResults.begin();
    //cout << "Melhor valor de minFreq = " << pMinFreq.getValue() << " com a percentagem :" << pMinFreq.getAverage() << endl;
    
    set<Parameter> orderedMaxFreqBestResults= MachineReportAnalyzer::calculateBestValueForParameter(_vMaxFreqParameters);
    Parameter pMaxFreq = *orderedMaxFreqBestResults.begin();

    set<Parameter> orderedNumberOfPeaksBestResults= MachineReportAnalyzer::calculateBestValueForParameter(_vNumberOfPeaksParameters);
    Parameter pNumberOfPeaksFreq = *orderedNumberOfPeaksBestResults.begin();

    
    
    ofstream os_reportFile;
    os_reportFile.open (machineReportsPath + "Performance.report");
    
    
    os_reportFile << "\t\t **** Performance Report **** \n" << endl;
    
    Test t = *allTestsByBestResult.begin();
    os_reportFile << "\t Best test with " << t.getSurvivorPeaksPc() << "% of success had the following test parameters: " <<endl;
    os_reportFile << "\t * Frame Size: " << t.getFrameSize() << endl;
    os_reportFile << "\t * Hop Size: " << t.getHopSize() << endl;
    os_reportFile << "\t * Minimum Frequency: " << t.getMinFrequencyOfPeaks() << endl;
    os_reportFile << "\t * Maximum Frequency: " << t.getMaxFrequencyOfPeaks() << endl;
    os_reportFile << "\t * Number Of Peaks: " << t.getNumberOfPeaksPerFrame() << "\n" << endl;

    
    os_reportFile << "\t Best parameter Values:" << endl;
    os_reportFile << "\t * Frame Size: (Por completar) \t Average Success:" << endl;
    os_reportFile << "\t * Hop Size: (Por completar) \t Average Success:" << endl;
    os_reportFile << "\t * Window: (Por completar) \t Average Success:" << endl;
    os_reportFile << "\t * Minimum Frequency: "<< pMinFreq.getValue() <<"\t Average Success:" << pMinFreq.getAverage()<< endl;
    os_reportFile << "\t * Maximum Frequency: "<< pMaxFreq.getValue() <<"\t Average Success:" << pMinFreq.getAverage()<< endl;
    os_reportFile << "\t * Number Of peaks: "<< pNumberOfPeaksFreq.getValue() <<"\t Average Success:" << pMinFreq.getAverage()<< endl;


    os_reportFile << "\n\n Ordered Success Results with only Minimum Frequency changes "<< endl;
    
    for (set<Parameter>::iterator minFreqIterator = orderedMinFreqBestResults.begin() ; minFreqIterator != orderedMinFreqBestResults.end(); minFreqIterator ++ ){
        Parameter p =(*minFreqIterator);
        os_reportFile << "\t * Minimum Frequency: " << p.getValue() <<  " Success Rate:  " << p.getAverage()<< endl;
    }
    
    os_reportFile << "\n\n Ordered Success Results with only Maximum Frequency changes "<< endl;

    
    for (set<Parameter>::iterator maxFreqIterator = orderedMaxFreqBestResults.begin() ; maxFreqIterator != orderedMaxFreqBestResults.end(); maxFreqIterator ++ ){
        Parameter p =(*maxFreqIterator);
        os_reportFile << "\t * Maximum Frequency: " << p.getValue() <<  " Success Rate:  " << p.getAverage()<< endl;
    }
    
    os_reportFile << "\n\n Ordered Success Results with only Number Of Peaks changes "<< endl;
    
    
    for (set<Parameter>::iterator numberOfPeaksIterator = orderedNumberOfPeaksBestResults.begin() ; numberOfPeaksIterator != orderedNumberOfPeaksBestResults.end(); numberOfPeaksIterator ++ ){
        Parameter p =(*numberOfPeaksIterator);
        os_reportFile << "\t * Number Of Peaks: " << p.getValue() <<  " Success Rate:  " << p.getAverage()<< endl;
    }

    os_reportFile.close();
    
    cout <<"Performance Report written on " << machineReportsPath + "Performance.report"<<endl;
    return 0;
}
