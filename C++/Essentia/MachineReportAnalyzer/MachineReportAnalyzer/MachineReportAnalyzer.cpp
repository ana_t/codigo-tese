//
//  MachineReportAnalyzer.cpp
//  MachineReportAnalyzer
//
//  Created by Ana on 22/04/15.
//  Copyright (c) 2015 Ana. All rights reserved.
//

#include "MachineReportAnalyzer.h"


set<Parameter> MachineReportAnalyzer::calculateBestValueForParameter(vector<Parameter> results){

    set<Parameter> orderedResults;
    for( vector<Parameter>::iterator resultIterator = results.begin(); resultIterator < results.end(); resultIterator++){
        orderedResults.insert(*resultIterator);
    }
    int counter = 0;
    for (set<Parameter>::iterator ordered = orderedResults.begin() ; ordered != orderedResults.end(); ordered ++ ){
        Parameter p =(*ordered);
        cout << "Index: " << counter <<  " Value in set:  :  " << p.getAverage()<< endl;
        counter ++;
    }

    
    return orderedResults;

}