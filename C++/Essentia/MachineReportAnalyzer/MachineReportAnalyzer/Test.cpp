//
//  Test.cpp
//  MachineReportAnalyzer
//
//  Created by Ana on 22/04/15.
//  Copyright (c) 2015 Ana. All rights reserved.
//

#include "Test.h"



Test::Test(int frameSize, int hopSize, int number_of_peaks_per_frame, int min_frequency_of_peaks, int max_frequency_of_peaks, double survivor_peaks_pc , int number_of_peaks_in_common, int total_number_of_peaks, double approximate_time_of_song, int number_of_frames){


    Test::frameSize = frameSize;
    Test::hopSize = hopSize;
    Test::number_of_peaks_per_frame = number_of_peaks_per_frame;
    Test::number_of_peaks_in_common = number_of_peaks_in_common;
    Test::total_number_of_peaks = total_number_of_peaks;
    Test::min_frequency_of_peaks =min_frequency_of_peaks;
    Test::max_frequency_of_peaks = max_frequency_of_peaks;
    Test::approximate_time_of_song = approximate_time_of_song;
    Test::number_of_frames = number_of_frames;
    Test::survivor_peaks_pc = survivor_peaks_pc;


}

double Test::getApproximateTimeOfSong(){
    return approximate_time_of_song;

}
int Test::getFrameSize(){
    return frameSize;
}
int Test::getHopSize(){
    return hopSize;
}
int Test::getMaxFrequencyOfPeaks(){
    return max_frequency_of_peaks;
}
int Test::getMinFrequencyOfPeaks(){
    return min_frequency_of_peaks;
}
int Test::getNumberOfFrames(){
    return number_of_frames;
}
int Test::getNumberOfPeaksInCommon(){
    return number_of_peaks_in_common;
}
int Test::getNumberOfPeaksPerFrame(){
    return number_of_peaks_per_frame;
}
double Test::getSurvivorPeaksPc(){
    return survivor_peaks_pc;
}
int Test::getTotalNumberOfPeaks(){
    return total_number_of_peaks;
}