//
//  MachineReportAnalyzer.h
//  MachineReportAnalyzer
//
//  Created by Ana on 22/04/15.
//  Copyright (c) 2015 Ana. All rights reserved.
//

#ifndef __MachineReportAnalyzer__MachineReportAnalyzer__
#define __MachineReportAnalyzer__MachineReportAnalyzer__

#include <stdio.h>
#include "Parameter.h"
#include <vector>
#include <set>
//#include "Parameter.h"
#endif /* defined(__MachineReportAnalyzer__MachineReportAnalyzer__) */

using namespace std;
class MachineReportAnalyzer{
    public:
        static set<Parameter> calculateBestValueForParameter(vector<Parameter> vector);
};