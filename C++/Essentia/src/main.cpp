//
//  main.cpp
//  XCodeExampleProject
//
//  Created by Nadine Kroher on 14/03/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//


#include "essentia.h"
#include "taglib.h"
#include "fftw3.h"
#include <essentia/algorithmfactory.h>
#include <essentia/essentiamath.h>
#include <essentia/pool.h>
#include <essentia/scheduler/network.h>
#include <essentia/streaming/algorithms/poolstorage.h>
#include <iostream>
#include <pthread.h>
#include <thread>
#include "DirectPeakComparator2Files.h"
#include <sys/stat.h>
#include <sys/types.h>

//#include <mgl2/mgl.h>

//draw plot's imports
//#include "mg2/mgl.h"

#define STATS_FILE "Spectral Peaks Stats.json"
#define SPECTRUM_FILE "Spectrum.json"
#define PEAKS_FILE "peaks.json"
#define ANALYZER_MODE true
#define NORMAL_MODE false
#define ANALIZER_INPUT_FORMAT 8
#define NORMAL_INPUT_FORMAT 7



using namespace std;
using namespace essentia::streaming;
using namespace essentia::scheduler;

string audioFilename;
int hopSize;
essentia::Real sampleRate = 44100.0 ;
bool execution_mode;
int frameSize;
ofstream os_reportFile;
static int min_Frequency;
static int max_Frequency;
static int number_of_peaks_per_frame;



void writePeakIntoFileIn2D(ofstream* os_outputFile, essentia::Real time, vector<essentia::Real> frequenciesOfPeaksAtThisTime){
    
    for (vector<essentia::Real>::iterator frequenciesIterator = frequenciesOfPeaksAtThisTime.begin(); frequenciesIterator != frequenciesOfPeaksAtThisTime.end(); frequenciesIterator++) {
        *os_outputFile << time << " " << *frequenciesIterator<< endl;
        

    }

}

void writeOutputToFileByFrame(vector<vector<essentia::Real>> frequencies, vector<vector<essentia::Real>> magnitudes, AudioProperties* audio, bool writePeaks2D){

    int frame_number = 0;
    essentia::Pool pool_to_be_filled;
    ofstream os_outputFile;
    if(writePeaks2D){
        os_outputFile.open ("./Peaks/Peaks2D.dat");

    }
    vector<vector<essentia::Real>>::iterator magnitudes_iterator = magnitudes.begin() ;
    //std::cout<<magnitudes.size()<< " -- Number of frames" << endl;
    audio->setFrameNumber((int)magnitudes.size());
    for(vector<vector<essentia::Real>>::iterator frequencies_iterator = frequencies.begin() ; frequencies_iterator != frequencies.end(); ++frequencies_iterator){
        

            essentia::Real frameTime = frame_number * (hopSize/sampleRate);

            pool_to_be_filled.add("Frame"+ to_string(frame_number)+".Frequencies", *frequencies_iterator);
            pool_to_be_filled.add("Frame"+ to_string(frame_number)+".Magnitudes", *magnitudes_iterator);
            pool_to_be_filled.add("Frame"+ to_string(frame_number)+".Time", frameTime );
            //std::cout<< (*output_vector_iterator).size()<< " <--  Número de picos da frame"<<std::endl;
            //cout << "Time for frame #" << frame_number<< "  =" << frameTime << "segundos" << endl;
            frame_number++;
            ++magnitudes_iterator;
            if(writePeaks2D)
                writePeakIntoFileIn2D(&os_outputFile, frameTime, *frequencies_iterator);
        
        
    }
    string pathToJson = "./Peaks/" + audio->getFilename() + "/" + audio->getFileType() + "/" + "Data/";
    string makePathToJson = "mkdir -p " + pathToJson;
    std::system(makePathToJson.c_str());
    essentia::standard::Algorithm* output_peaks = essentia::standard::AlgorithmFactory::create("YamlOutput","filename", pathToJson + to_string(frameSize) + "." + to_string(hopSize) + "." + to_string(number_of_peaks_per_frame) + "." + to_string(min_Frequency) + "." + to_string(max_Frequency) + PEAKS_FILE);
    // write results to file
    cout << "-------- writing results to file " << "./Peaks/"+ audio->getFilename()+ + "/"+ audio->getFileType() + "/" + to_string(frameSize) + "." + to_string(hopSize) + "." + to_string(number_of_peaks_per_frame) + "."+ to_string(min_Frequency) + "." + to_string(max_Frequency) + PEAKS_FILE << "  --------------------" << endl;
    

    output_peaks->input("pool").set(pool_to_be_filled);
    output_peaks->compute();
    //cleaning resource
    delete output_peaks;
    if(writePeaks2D){
        os_outputFile.close();
        //chama gnuplots para traçar os picos num gráfico.
        string plot_program_and_arguments ="./Peaks/Peaks2D.gnuplot";
        std::system(plot_program_and_arguments.c_str());

    }
    string pathToPlot = "./Peaks/" + audio->getFilename() + "/" + audio->getFileType() + "/Plots/Peaks/";
    string mkdirsToPlot ="mkdir -p "+ pathToPlot;
    std::system(mkdirsToPlot.c_str());
    
    string basePathToAudioInformation = pathToPlot + to_string(frameSize) + "." + to_string(hopSize) + "." + to_string(number_of_peaks_per_frame) + "." +to_string(min_Frequency) + "." + to_string(max_Frequency);

    string renamePlotToAppropriateName = "mv ./Peaks/peaks2D.png " + basePathToAudioInformation + ".png";
    string renameDatToAppropriateName = "mv ./Peaks/Peaks2D.dat " + basePathToAudioInformation + ".dat";

    std::system(renamePlotToAppropriateName.c_str());
    std::system(renameDatToAppropriateName.c_str());

}

Pool searchForPeaks(AudioProperties* audio, int frameSize, int hopSize, essentia::Real sampleRate){
    
    //To accomplish the Speactral peaks, one needs to do the following algorithms:
    // audioloader (in case of stereo sound) and Monoloader in case of an mono output  -> framecutter -> Windowing -> Spectrum -> Spectral Peaks
    
    AlgorithmFactory& factory = essentia::streaming::AlgorithmFactory::instance();
    
    Algorithm * algAudio = factory.create("MonoLoader", "filename", audio->getFilePath(),"sampleRate", sampleRate);
    
    Algorithm * algFrameCutter = factory.create("FrameCutter","frameSize", frameSize,"hopSize", hopSize, "startFromZero", true);
    Algorithm * algWindowing = factory.create("Windowing", "type", "blackmanharris92");
    Algorithm* algSpectrum  = factory.create("Spectrum");
    
    Algorithm *algSpectralPeaks = factory.create("SpectralPeaks","maxFrequency", max_Frequency, "maxPeaks", number_of_peaks_per_frame, "minFrequency", min_Frequency, "orderBy", "magnitude");
    
    // This algorithms will be used together, as a pipeline, to apply the changes in the wanted order. The order and the changes chosen earlier will give as the Spectral Peaks. The following code will be encharge of the scheduling of the functions. First the MonoLoader will pass the information to the frameCutter, and so on...
    
    cout << "-------- connecting the algorithms --------" << endl;
    
    // Sending Audio -> FrameCutter
    algAudio -> output("audio") >> algFrameCutter->input("signal");
    // FrameCutter -> Windowing
    algFrameCutter->output("frame")  >>  algWindowing->input("frame");
    
    //Windowing -> Spectrum
    algWindowing->output("frame") >>  algSpectrum->input("frame");
    // Spectrum -> SpectralPeaks
    algSpectrum->output("spectrum")  >>  algSpectralPeaks->input("spectrum");
    
    
    // defining the save of the output from the algSpectralPeaks to the pool under the name of:
    Pool pool;
    algSpectralPeaks->output("frequencies") >> PC(pool, "SpectralPeaksFrequencies");
    algSpectralPeaks->output("magnitudes") >> PC(pool, "SpectralPeaksMagnitudes");
    //algSpectrum->output("spectrum") >> PC(spectrum_pool, "Spectrum");
    
    cout << "-------- started the processing of " <<  audio->getFilename()<< " - " << audio->getFileType() << " --------" << endl;
    // The network/pipeline creation
    Network pipeline(algAudio);
    
    // start the operations on the pipeline
    pipeline.run();
    
    cout << "-------- Cleaning resources from this iteration--------" << endl;
    
    pipeline.clear();
    return pool;
    
}

void calculateStatsFromPool(essentia::Pool pool_to_be_analyzed, AudioProperties audio){

    // aggregate the results to have stats. This will be saved in a file called "Spectral Peaks Stats.json"
    
    essentia::Pool aggrPool; // the pool with the aggregated values
    const char* stats[] = { "mean", "var", "min", "max" };
    
    essentia::standard::Algorithm* aggr = essentia::standard::AlgorithmFactory::create("PoolAggregator","defaultStats"+ audio.getFilename(), essentia::arrayToVector<string>(stats));
    
    aggr->input("input").set(pool_to_be_analyzed);
    aggr->output("output").set(aggrPool);
    aggr->compute();
    delete aggr;


}
/**************************************************************************************************************************
***************************************************************************************************************************
***********************************************         MAIN        *******************************************************
***************************************************************************************************************************
***************************************************************************************************************************/




int main(int argc, const char * argv[]) {
    
    if (argc < NORMAL_INPUT_FORMAT || argc > ANALIZER_INPUT_FORMAT) {
        cout << "Inserted as argument 1 :  = " << argv[1] << endl;
        cout << "Inserted as argument 2) = " << argv[2] << endl;
        cout << "ERROR: incorrect number of arguments." << endl;
        cout << "Usage: " << argv[0] << " path_original_audio frame_size hop_size number_of_peaks_per_frame min_frequency_of_peaks max_frequency_of_peaks [path_same_audio_with_noise] \n The optional field is given if a comparasion of the peaks between two files is wanted" << endl;
        exit(1);
    }
    

    
    
    
    /////// PARAMS //////////////
    frameSize = atoi(argv[2]); //samples per frame
    hopSize = atoi(argv[3]);    // number of samples to jump to take next frame
    number_of_peaks_per_frame = atoi(argv[4]);
    min_Frequency = atoi(argv[5]);
    max_Frequency = atoi(argv[6]);
    
    //sampleRate = 44100.0; //44.1 kHz of samples per second
    //sampleRateForPeaks = sampleRate/2;
    
    //essentia::Pool spectrum_pool; // the pool with the aggregated values

    AudioProperties* audio1 =  new AudioProperties(argv[1]);
    audio1->setHopSize(hopSize);
    vector<AudioProperties*> audioFiles;
    
    audioFiles.push_back(audio1);
    
    if (argc == NORMAL_INPUT_FORMAT )
        execution_mode = NORMAL_MODE;
    
    else{
        execution_mode = ANALYZER_MODE;
        AudioProperties* audio2 =  new AudioProperties(argv[7]);
        audio2->setHopSize(hopSize);
        audioFiles.push_back(audio2);
        string reportPath ="Reports/";
        string reportExtension = ".report";
        string originalFileFolder = audio1->getFilename()+ "/";
        string noiseTypeFolder = audio2->getFileType() + "/";
        string reportName = to_string(frameSize) + "." + to_string(hopSize) + "." + to_string(number_of_peaks_per_frame) + "." +to_string(min_Frequency) + "." + to_string(max_Frequency);
        reportPath+= originalFileFolder + noiseTypeFolder;
        //makes dirs to the report if they do not exist
        string mkdirsToReport ="mkdir -p "+ reportPath;
        std::system(mkdirsToReport.c_str());
        reportPath+= reportName+reportExtension;

        os_reportFile.open (reportPath);
        // Writing of the first lines of the report
        os_reportFile << " *************************************** REPORT OF THE COMPARISON OF THE FOLLOWING TWO FILES ***************************************\n\t > " << audio1->getFilename() << "\n\t > Noise Version : " << audio2->getFileType()<< endl;
        os_reportFile << " \n\n Parameters For the Peak Search \n\t > Number Of Peaks Per Frame : " << number_of_peaks_per_frame << "\n\t > Minimum Frequency for the Peaks : " << min_Frequency << "\n\t > Maximum Frequency for the Peaks : " << max_Frequency <<  "\n\t > HopSize : " << hopSize << endl;


    }

    // To register the algorithms in the factory (ies)
    essentia::init();
    
    
    //calculating the peaks for each file - *TODO* this part will be running in multithreading to improve results in case of two files
    
    for(vector<AudioProperties*>::iterator audioFilesIterator = audioFiles.begin() ; audioFilesIterator != audioFiles.end() ; audioFilesIterator ++){
        cout << "--- Feature Extraction and its storage " << endl;
        time_t start_time = time(NULL);
        Pool pool = searchForPeaks(*audioFilesIterator, frameSize, hopSize, sampleRate);
        std::this_thread::sleep_for(std::chrono::seconds(3));
        //audioFilesIterator->setPool(pool);
        //cout<< "Descriptors : " << pool.descriptorNames() << endl;
        (*audioFilesIterator)->setFrameNumber((int)pool.value<vector<vector<essentia::Real>>>("SpectralPeaksFrequencies").size());
        //writeOutputToFileByFrame(pool.value<vector<vector<essentia::Real>>>("SpectralPeaksFrequencies"), pool.value<vector<vector<essentia::Real>>>("SpectralPeaksMagnitudes"), *audioFilesIterator , true);
        (*audioFilesIterator)->setPool(pool);

        cout << "--- Time of feature extraction and its storage  (elapsed) : " << time(NULL) - start_time <<" seconds\n" <<endl;
    }
    
        essentia::shutdown();
    
    if (argc != NORMAL_INPUT_FORMAT ){
        DirectPeakComparator2Files::comparePeaks(&os_reportFile, audioFiles.at(0), audioFiles.at(1), hopSize, sampleRate,  frameSize , number_of_peaks_per_frame,  min_Frequency,  max_Frequency);
        os_reportFile.close();
    }
    for(vector<AudioProperties*>::iterator audioFilesIterator = audioFiles.begin() ; audioFilesIterator != audioFiles.end() ; audioFilesIterator ++){
        (*audioFilesIterator)->deletePeaks();
        delete *audioFilesIterator;
    }
    audioFiles.clear();
    
    
    return 0;
}
