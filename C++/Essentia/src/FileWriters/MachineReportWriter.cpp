//
//  MachineReportWriter.cpp
//  Spectrogram
//
//  Created by Ana on 22/04/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#include "MachineReportWriter.h"


void MachineReportWriter::writeMachineReport(string pathToReport, int frameSize, int hopSize, int number_of_peaks_per_frame, int min_frequency_of_peaks, int max_frequency_of_peaks, double survivor_peaks_pc , unsigned long number_of_peaks_in_common, unsigned long total_number_of_peaks, essentia::Real approximate_time_of_song, unsigned long number_of_frames){

    ofstream os_reportFile;
    
    os_reportFile.open (pathToReport);
    
    if(!os_reportFile.is_open())
        cout<< "ERROR: For some reason, the machine report can't be written" << endl;
    //Writting of the machine report. Every important parameter or analysis result will be written in a different line
    else {
        
        os_reportFile << frameSize << endl;
        os_reportFile << hopSize << endl;
        os_reportFile << number_of_peaks_per_frame << endl;
        os_reportFile << min_frequency_of_peaks << endl;
        os_reportFile << max_frequency_of_peaks << endl;
        os_reportFile << survivor_peaks_pc << endl;
        os_reportFile << number_of_peaks_in_common << endl;
        os_reportFile << total_number_of_peaks << endl;
        os_reportFile << approximate_time_of_song << endl;
        os_reportFile << number_of_frames << endl;
    
    }
    os_reportFile.close();

}
