//
//  MachineReportWriter.h
//  Spectrogram
//
//  Created by Ana on 22/04/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#ifndef __Spectrogram__MachineReportWriter__
#define __Spectrogram__MachineReportWriter__

#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include "essentia.h"

#endif /* defined(__Spectrogram__MachineReportWriter__) */
using namespace std;
using namespace essentia;


class MachineReportWriter{

//# path_original_audio frame_size hop_size number_of_peaks_per_frame min_frequency_of_peaks max_frequency_of_peaks [path_same_audio_with_noise]
    public:
    static void writeMachineReport(string pathToReport, int frameSize, int hopSize, int number_of_peaks_per_frame, int min_frequency_of_peaks, int max_frequency_of_peaks, double survivor_peaks_pc , unsigned long number_of_peaks_in_common, unsigned long total_number_of_peaks, essentia::Real approximate_time_of_song, unsigned long number_of_frames);

};