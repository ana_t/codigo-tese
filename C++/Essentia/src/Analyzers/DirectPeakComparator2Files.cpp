//
//  DirectPeakComparator2Files.cpp
//  Spectrogram
//
//  Created by Ana on 15/04/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#include "DirectPeakComparator2Files.h"

#include <stdio.h>
#define NUMBER_OF_SECONDS_TO_CLASS 5
#define DUMMY_SECONDS_ADDED_TO_AUDIO 2

    
/*Method that compares two audio files directly. 
 *
 *IMPORTANT* This comparator only works with files that have identical music positions. An example of the good 
 *usage of this method is comparing two identical music files where one has been injected with noise directly.
 *This will be important to make an preliminar study on the peak's characteristics that resist to noise for example.
 *
 */
bool DirectPeakComparator2Files::comparePeaks(ofstream* os_reportFile, AudioProperties* audio1, AudioProperties* audio2, int hopSize,int sampleRate, int frameSize, int number_of_peaks_per_frame, int min_frequency_of_peaks, int max_frequency_of_peaks ){
        
        //getting the frequencies from both audios. This is done by searching the previous made pools.
    
    
        int  numberOfSurvivors = 0;
        double percentageOfSurvival = 0;
        size_t numberOfFramesAudio1 = audio1->getFrameNumber() ,numberOfFramesAudio2 = audio2->getFrameNumber();
    
        vector<essentia::Real> realVector_survivorPeaks_byTime;
        vector<essentia::Real> realVector_survivorPeaks_byFrequency;
        vector <Peak> commonPeaks; //
        //in case, some of the files has been injected with more sound than the music's length than the comparator will only run on
        //the frames with corresponding frames in the other file.
        size_t numberOfFramesToAnalyze = (numberOfFramesAudio1 < numberOfFramesAudio2) ?numberOfFramesAudio1:numberOfFramesAudio2;
        vector<string> fingerprintsOSample = audio2->getFingerprints();
        size_t numberOfSamplePeaks = audio2->getPeaks().size();
        size_t numberOfOriginalPeaks = audio1->getPeaks().size();

        //TODO: MUDAR getNumberOfFingerprints () quando deixar de haver um fingerprint por pico.
        size_t numberOfPeaks = (numberOfSamplePeaks < numberOfOriginalPeaks)? numberOfSamplePeaks:numberOfOriginalPeaks;

    
    /***************************************************************************************************************************
     ***************************************************************************************************************************
     ***********************************************         SEARCH BY FINGERPRINT        **************************************
     ***************************************************************************************************************************
     ***************************************************************************************************************************/
    cout << "--- Search for the collected fingerprints from noisy sample in the 'DB' "<< endl;

    time_t start_time = time(NULL);
    

    for(vector<string>::iterator sample_fingerprint_iterator = fingerprintsOSample.begin() ; sample_fingerprint_iterator < fingerprintsOSample.end(); sample_fingerprint_iterator ++ ){
    
        if(audio1->checkForPeakByFingerprint(*sample_fingerprint_iterator)){
            numberOfSurvivors++;
            
            fingerprint fsample = audio2->getFingerprintStructFromKey(*sample_fingerprint_iterator);
            fingerprint foriginal = audio1->getFingerprintStructFromKey(*sample_fingerprint_iterator);
            
            if(fsample.frequency1 != foriginal.frequency1 || fsample.frequency2 != foriginal.frequency2){
                
                cout << " \n DirectPeakComparator >>> FALSE POSITIVE MATCH!!!" << endl;
                cout << "-Peaks Data-" <<endl;

                cout <<" F1Original = " << foriginal.frequency1 <<" F1Sample = "<< fsample.frequency1 << endl;
                cout <<" F2Original = " << foriginal.frequency2 <<" F2Sample = "<< fsample.frequency2 << endl;
                cout <<" T1Original = " << foriginal.time1 <<" T1Sample = "<< fsample.time1 << endl;
                cout <<" T2Original = " << foriginal.time2 <<" T2Sample = "<< fsample.time2 << endl;

                cout << "-Fingerprint Data-" <<endl;
                
                cout <<" Fingerprint Original = " <<  foriginal.fingerprint <<" Fingerprint Sample = "<< fsample.fingerprint << endl;
/*
                cout <<" ORIGINAL : foriginal.frequency1 * (foriginal.time2 - foriginal.time1) = " << foriginal.frequency1 * (foriginal.time2 - foriginal.time1) <<" SAMPLE : foriginal.frequency1 * (foriginal.time2 - foriginal.time1) = "<< fsample.frequency1 * (fsample.time2 - fsample.time1) << endl;
                cout <<" ORIGINAL : fsample.frequency2/4.0 = " << foriginal.frequency2/4.0 <<" SAMPLE : fsample.frequency2/4.0 = "<< fsample.frequency2/4.0 << endl;
                cout <<" ORIGINAL : foriginal.frequency1 * (foriginal.time1 * foriginal.time2) - foriginal.frequency2= " << foriginal.frequency1 * (foriginal.time2 - foriginal.time1) - foriginal.frequency2 <<" SAMPLE : foriginal.frequency1 * (foriginal.time1 * foriginal.time2) - foriginal.frequency2/4.0 = "<< fsample.frequency1 * ( fsample.time2- fsample.time1 ) - fsample.frequency2/4.0 << "\n" <<endl;
                */
                //     essentia::Real result = (frequency_peak_1 * frequency_peak_2)*(time_peak_1 * time_peak_2 + 3);

            }
            /*
                else
                    cout << " DirectPeakComparator >>> Correct Match! > Fo size = " << sizeof(foriginal) << "  Fsample size = "<< sizeof(fsample)  << " FO fingerprint key= " << foriginal.fingerprint << " Size of fingerprint key = "<< sizeof(foriginal.fingerprint)<< "\n Freq1 = "<< foriginal.frequency1 << " Freq2 = " <<  foriginal.frequency2 << endl;
*/
            
            //Peak * p = *sample_fingerprint_iterator;
            //realVector_survivorPeaks_byFrequency.push_back(*(p->getFrequency()));
            //realVector_survivorPeaks_byTime.push_back(*(p->getTime()));

        }
    }
    cout << "--- Time of the search for the collected fingerprints from noisy sample in the 'DB'  (elapsed) : " << time(NULL) - start_time <<" seconds" << endl;

    
    percentageOfSurvival = (double)numberOfSurvivors/(double)numberOfPeaks;
    //global stats
    string report_line = "\n\n>>>>>>>>Direct peak comparasion between Audio: " +  audio1->getFilename() + " and its >" + audio2->getFileType() + "< noise version\n";

    cout << report_line << endl;
    *os_reportFile << report_line;

    
    cout << " >>Global stats" << endl;
    
    report_line = "\t =>  Survivor fingerprints = " + to_string(percentageOfSurvival) + "  \n\t => number of fingerprints in common = " + to_string(numberOfSurvivors) + " \n\t => number of fingerprints in total = "+ to_string(numberOfPeaks);
    cout << report_line << endl;
    *os_reportFile << report_line;
    //local stats
    
    vector<double> vectorD_frequencyDistributionOfFrequencies = StatsMaker::getFrequencyDistribution(realVector_survivorPeaks_byFrequency,2000, 0, 20000, numberOfSurvivors );
    essentia::Real approximateTimeOfAudio = audio1->getFrameNumber()* ((essentia::Real)hopSize/(essentia::Real)sampleRate)+ DUMMY_SECONDS_ADDED_TO_AUDIO;
    report_line =  "\n\n >>Time Information\n\t => Approximate time of Audio = " + to_string(approximateTimeOfAudio)  + "  \n\t => Number of frames : " + to_string(numberOfFramesToAnalyze) + "  \n\t => HopSize : " +  to_string(hopSize) + "  \n\t => Sample rate : " + to_string(sampleRate);
    
    cout << report_line << endl;
    *os_reportFile << report_line;
 
        return true;
        
    }


