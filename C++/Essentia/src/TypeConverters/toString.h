//
//  toString.h
//  Spectrogram
//
//  Created by Ana on 15/04/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#ifndef __Spectrogram__toString__
#define __Spectrogram__toString__

#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>


#endif /* defined(__Spectrogram__toString__) */

using namespace std;

class toString{

public:
    static string intToString (int a);
    static string doubleToString (double dbl);

};