//
//  AudioProperties.cpp
//  Spectrogram
//
//  Created by Ana on 08/04/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#include "AudioProperties.h"
#include "Fingerprinter.h"
#include <assert.h>     /* assert */

AudioProperties::AudioProperties(string filePath){

    _strFilePath = filePath;
    setFileName(filePath);
    _iFrame_number = 0;

}

AudioProperties::~AudioProperties(){

    peaks.clear();
    
}


void AudioProperties::setFileName(string filepath){
    
    size_t aux_position_of_slash =  filepath.find("/");
    size_t aux_position_of_previous_slash =  -1;

    string aux_file_name = filepath;
    while(aux_position_of_slash != std::string::npos){
        _strFileType = aux_file_name;
        aux_file_name = aux_file_name.substr(aux_position_of_slash+1);
        aux_position_of_slash =  aux_file_name.find("/");
        
    }
    _strFileName = aux_file_name;
    aux_position_of_previous_slash =_strFileType.find("/");
    _strFileType = _strFileType.substr(0,aux_position_of_previous_slash);
    //_strFileType = _strFileType.substr(aux_position_of_previous_slash);
    aux_position_of_slash =  filepath.find("/");
    //cout << "FileName  = " << aux_file_name << " Type = " << _strFileType << endl;
    
}

void AudioProperties::setFrameNumber(int size){

    _iFrame_number = size;
}


void AudioProperties::setPool(essentia::Pool pool){


    _poolPool = pool;
    savePeaksFromPoolInVector(_poolPool);
    
    //cout << getPool().descriptorNames()<< endl ;
}
string AudioProperties::getFilename() {

    return _strFileName;
}

string AudioProperties::getFilePath(){

    return _strFilePath;
}

string AudioProperties::getFileType(){
    
    return _strFileType;
}

Pool AudioProperties::getPool(){

    return _poolPool;
}

int AudioProperties::getFrameNumber(){

    return _iFrame_number;
}


void AudioProperties::calculateFingerprintsForSong(vector<vector<essentia::Real>> frequencies_for_all_frames,vector<vector<essentia::Real>> magnitudes_for_all_frames ){
    
    bool isOriginalSong  = _strFileType.compare("Original") == 0;

    int frame = 0;

    vector<vector<essentia::Real>>::iterator magnitudes_iterator = magnitudes_for_all_frames.begin() ;
    //for every frame
    for(vector<vector<essentia::Real>>::iterator frequencies_iterator = frequencies_for_all_frames.begin() ; frequencies_iterator != frequencies_for_all_frames.end(); frequencies_iterator++){
        
        essentia::Real current_frame_time = frame * (_iHopSize/_rSampleRate);
        //gets magnitudes of that frame
        vector<essentia::Real>::iterator magnitudes_of_current_frame = (*magnitudes_iterator).begin();
        
        
        //for every frequency data on frame
        for (vector<essentia::Real>::iterator frequencies_of_current_frame = (*frequencies_iterator).begin(); frequencies_of_current_frame != (*frequencies_iterator).end(); frequencies_of_current_frame ++ ){
            essentia::Real current_frequency = *(frequencies_of_current_frame);
            essentia::Real current_magnitude = *(magnitudes_of_current_frame);
            //cout << " freq " << current_frequency << " mag "<< current_magnitude  << "; current frame =  " << frame <<  " current time = " << current_frame_time << " current hop = " << _iHopSize << " sample =  " << _rSampleRate <<  endl;
            
            Peak p1 = Peak(current_frequency,current_magnitude, current_frame_time, frame);
            Peak *peak = new Peak(p1);
            peaks.push_back(peak);
            
            
            //for each following 2 frames, make fingerprints with the combination of their peaks
            for (int nextFrame = 1 ; frequencies_iterator !=frequencies_for_all_frames.end() && nextFrame < 5; nextFrame ++ ){
                
                //for each peak in frame
                for (vector<essentia::Real>::iterator frequencies_of_next_frame = (*(frequencies_iterator + nextFrame)).begin(); frequencies_of_next_frame != (*(frequencies_iterator + nextFrame)).end(); frequencies_of_next_frame ++ ){

                essentia::Real next_frequency = *(frequencies_of_next_frame);
                essentia::Real next_frame_time = (frame + nextFrame) * (_iHopSize/_rSampleRate);
                string fingerprint = Fingerprinter::makeFingerprint(current_frame_time, next_frame_time, current_frequency ,next_frequency);
                    struct fingerprint f;
                    f.frequency1 = current_frequency;
                    f.frequency2 = next_frequency;
                    f.time1 = current_frame_time;
                    f.time2 =next_frame_time;
                    f.fingerprint = fingerprint;
                    fingerprint_to_fingerprintStruct[fingerprint] = f;
                    if (isOriginalSong){
                        assert(fingerprints.count(fingerprint) ==0);
                    fingerprints[fingerprint] = this;
                        
                        
                    }
                else fingerprintsForSample.push_back(fingerprint);
                    
                    
                }
                
            }
            magnitudes_of_current_frame++;
            
            
        }
        ++magnitudes_iterator;
        ++frame;
        
    }
    

}
fingerprint AudioProperties::getFingerprintStructFromKey(string  fingerprint){

    return fingerprint_to_fingerprintStruct[fingerprint];

}


void AudioProperties::savePeaksFromPoolInVector(essentia::Pool pool){
    
    //gets data from pool for every frame - each vector in the vector is data from a single frame
    vector<vector<essentia::Real>> frequencies_for_all_frames = pool.value<vector<vector<essentia::Real>>>("SpectralPeaksFrequencies");
    vector<vector<essentia::Real>> magnitudes_for_all_frames = pool.value<vector<vector<essentia::Real>>>("SpectralPeaksMagnitudes");
    
    
    calculateFingerprintsForSong(frequencies_for_all_frames, magnitudes_for_all_frames );
    
   }

vector<Peak*> AudioProperties::getPeaks(){
    //cout << "**************************************************\n I have " << peaks.size() << " peaks *****************************\n **************************************************" << endl;
    return peaks;

}

void AudioProperties::setHopSize(int hop){
    _iHopSize = hop;
}


bool AudioProperties::checkForPeakByFingerprint(string fingerprint){
    
    map<string , AudioProperties*>::iterator fingerprintsIterator = fingerprints.find(fingerprint);
        return fingerprintsIterator != fingerprints.end();

}


vector< string > AudioProperties::getFingerprints(){

    return fingerprintsForSample;


}


//TODO: verificar se é para tirar
void AudioProperties::deletePeaks(){
    
    for (vector<Peak*>::iterator peakIterator = peaks.begin() ; peakIterator < peaks.end() ; peakIterator ++ ){
    
        delete *peakIterator;
    }
    peaks.clear();
}

