#! /usr/bin/gnuplot

spectrogram_dat=system("echo $MUSIC_SPECTROGRAM_DAT")
peaks_dat=system("echo $PEAKS_DAT")
outputfile=system("echo $OUTPUT_FILE")
set pm3d map
set palette model XYZ rgbformulae 7,5,15
splot spectrogram_dat
set term png size 1200,700
set output outputfile
set xlabel 'Time [s]'
set ylabel 'Frequency [Hz]'
set cblabel 'Intensity [dB]'
#set cbrange [0 to GPVAL_DATA_Z_MAX]
#set xrange [GPVAL_DATA_X_MIN to GPVAL_DATA_X_MAX]
#set yrange [3.2 to 3.45]
unset key
splot spectrogram_dat with pm3d, peaks_dat using 1:2:(0.0) with points linecolor rgb "white"
unset output


