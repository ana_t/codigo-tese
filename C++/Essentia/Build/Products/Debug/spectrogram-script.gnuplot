#! /usr/bin/gnuplot

spectrogram_dat=system("echo $MUSIC_SPECTROGRAM_DAT")
spectrogram_file=system("echo $SPECTROGRAM_FILE")
set pm3d map
set palette model XYZ rgbformulae 7,5,15
splot spectrogram_dat
#set terminal png crop
set term png size 1200,700
set output spectrogram_file
set xlabel 'Time [s]'
set ylabel 'Frequency [Hz]'
set cblabel 'Intensity [dB]'
#set cbrange [0 to GPVAL_DATA_Z_MAX]
#set xrange [GPVAL_DATA_X_MIN to GPVAL_DATA_X_MAX]
#set yrange [3.2 to 3.45]
unset key
replot
unset output


