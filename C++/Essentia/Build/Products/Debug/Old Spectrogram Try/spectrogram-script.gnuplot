#! /usr/local/bin/gnuplot
# define o grafico como um grafico de cor 2D para dados 3D 
# desenha um grafico preliminar para carregar definiçoes de range -- confirmar
#define que o output sera em png e que ira retirar toda a transparencia a volta dos cantos
# define output filename
#define a legenda do eixo do x
#define a legenda do eixo do y 
#define a legenda do eixo do z -- cores
# confirmar!!!!
# confirmar!!!!
# esconde linha com nome do ficheiro do plot
# volta a desenhar o grafico mas agora com as definiçoes dadas anteriormente. Escreve agora no .png.
# manda fechar o ficheiro de output


set pm3d map 
set terminal png crop
set output 'spectrogram_plot.png'
set xlabel 'Time [s]'  
set ylabel 'Frequency [Hz]'
set cblabel 'Amplitude [dB]'
set cbrange [-120 to 80]
set xrange [0.0 to 30.0]
 # confirmar!!!!
set yrange [0.0 to 20000.0]
unset key 
splot 'spectrogram_data.dat'  
unset output 