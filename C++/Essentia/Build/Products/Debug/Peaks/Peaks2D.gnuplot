#! /usr/local/bin/gnuplot

#set term dumb
#plot "./Peaks/Peaks2D.dat" with points
set terminal png
set term png size 1200,700 #x pixels by y pixels
set output './Peaks/peaks2D.png'

set xlabel "Time (s)"
set ylabel "Frequency (Hz)"
plot "./Peaks/Peaks2D.dat" with points
