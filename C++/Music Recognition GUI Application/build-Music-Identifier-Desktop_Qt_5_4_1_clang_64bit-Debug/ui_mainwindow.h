/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *Identify_button;
    QPushButton *choose_sample_button;
    QTextBrowser *output_text_Browser;
    QLabel *Sample_Path_Label;
    QPlainTextEdit *frameSize_textField;
    QLabel *label;
    QPlainTextEdit *hopSize_textField;
    QLabel *label_2;
    QPlainTextEdit *numberOfPeaksPerFrame_textField;
    QLabel *label_3;
    QPlainTextEdit *minFreq_textField;
    QLabel *label_4;
    QPlainTextEdit *maxFreq_textField;
    QLabel *label_5;
    QPushButton *choose_original_button;
    QLabel *Original_Path_Label;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(939, 427);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        Identify_button = new QPushButton(centralWidget);
        Identify_button->setObjectName(QStringLiteral("Identify_button"));
        Identify_button->setGeometry(QRect(460, 290, 113, 32));
        choose_sample_button = new QPushButton(centralWidget);
        choose_sample_button->setObjectName(QStringLiteral("choose_sample_button"));
        choose_sample_button->setGeometry(QRect(30, 50, 141, 32));
        output_text_Browser = new QTextBrowser(centralWidget);
        output_text_Browser->setObjectName(QStringLiteral("output_text_Browser"));
        output_text_Browser->setGeometry(QRect(40, 90, 891, 191));
        output_text_Browser->setTabStopWidth(140);
        Sample_Path_Label = new QLabel(centralWidget);
        Sample_Path_Label->setObjectName(QStringLiteral("Sample_Path_Label"));
        Sample_Path_Label->setGeometry(QRect(180, 60, 751, 16));
        frameSize_textField = new QPlainTextEdit(centralWidget);
        frameSize_textField->setObjectName(QStringLiteral("frameSize_textField"));
        frameSize_textField->setGeometry(QRect(40, 290, 71, 21));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(40, 320, 71, 16));
        hopSize_textField = new QPlainTextEdit(centralWidget);
        hopSize_textField->setObjectName(QStringLiteral("hopSize_textField"));
        hopSize_textField->setGeometry(QRect(120, 290, 71, 21));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(120, 320, 71, 16));
        numberOfPeaksPerFrame_textField = new QPlainTextEdit(centralWidget);
        numberOfPeaksPerFrame_textField->setObjectName(QStringLiteral("numberOfPeaksPerFrame_textField"));
        numberOfPeaksPerFrame_textField->setGeometry(QRect(200, 290, 71, 21));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(200, 320, 71, 31));
        label_3->setTextFormat(Qt::AutoText);
        label_3->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        label_3->setWordWrap(true);
        minFreq_textField = new QPlainTextEdit(centralWidget);
        minFreq_textField->setObjectName(QStringLiteral("minFreq_textField"));
        minFreq_textField->setGeometry(QRect(280, 290, 71, 21));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(280, 320, 71, 31));
        label_4->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        label_4->setWordWrap(true);
        maxFreq_textField = new QPlainTextEdit(centralWidget);
        maxFreq_textField->setObjectName(QStringLiteral("maxFreq_textField"));
        maxFreq_textField->setGeometry(QRect(360, 290, 71, 21));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(360, 320, 71, 31));
        label_5->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        label_5->setWordWrap(true);
        choose_original_button = new QPushButton(centralWidget);
        choose_original_button->setObjectName(QStringLiteral("choose_original_button"));
        choose_original_button->setGeometry(QRect(30, 20, 141, 32));
        Original_Path_Label = new QLabel(centralWidget);
        Original_Path_Label->setObjectName(QStringLiteral("Original_Path_Label"));
        Original_Path_Label->setGeometry(QRect(180, 30, 751, 16));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 939, 22));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Music Identifier", 0));
        Identify_button->setText(QApplication::translate("MainWindow", "IDENTIFY ! ", 0));
        choose_sample_button->setText(QApplication::translate("MainWindow", "Choose Sample", 0));
        Sample_Path_Label->setText(QString());
        frameSize_textField->setPlainText(QApplication::translate("MainWindow", "512", 0));
        label->setText(QApplication::translate("MainWindow", "Frame Size", 0));
        hopSize_textField->setPlainText(QApplication::translate("MainWindow", "256", 0));
        label_2->setText(QApplication::translate("MainWindow", "Hop Size", 0));
        numberOfPeaksPerFrame_textField->setPlainText(QApplication::translate("MainWindow", "15", 0));
        label_3->setText(QApplication::translate("MainWindow", "#Peaks per    Frame", 0));
        minFreq_textField->setPlainText(QApplication::translate("MainWindow", "80", 0));
        label_4->setText(QApplication::translate("MainWindow", "Minimum Frequency", 0));
        maxFreq_textField->setPlainText(QApplication::translate("MainWindow", "10000", 0));
        label_5->setText(QApplication::translate("MainWindow", "Minimum Frequency", 0));
        choose_original_button->setText(QApplication::translate("MainWindow", "Choose Original", 0));
        Original_Path_Label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
