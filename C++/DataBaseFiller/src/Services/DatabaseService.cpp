//
//  DatabaseService.cpp
//  DataBaseFiller
//
//  Created by Ana on 13/05/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//
#include "DatabaseService.h"


//static sql::mysql::MySQL_Driver *driver;
//static sql::Connection *connection;



string const DEFAULT_DB =  "./Database/db_current";



bool DatabaseService::insertSongInDatabase(  AudioProperties *pAudioProperties){
    
    unsigned int previous_number_of_songs_database = _mapMusicByID.size();
    assert( " Music is already in database! " && _mapMusicByID.find(pAudioProperties->getMusic()->getID()) == _mapMusicByID.end()  ) ;
    if(_mapMusicByID.find(pAudioProperties->getMusic()->getID()) != _mapMusicByID.end())
        return false;
    
    assert("ID of song to be added is empty " &&  pAudioProperties->getMusic()->getID() != "");

    _mapMusicByID[pAudioProperties->getMusic()->getID()] = pAudioProperties;
    //adds fingerprints to the map of fingerprints To Songs
    
    assert("DatabaseService >> insertSongInDatabase: The song you are trying to insert has no fingerprints! " && pAudioProperties->getNumberOfFingerprints() != 0);
    
    set<string> fingerprints = pAudioProperties->getFingerprints();
    
    for ( set<string>::iterator music_fingerprints_iterator = fingerprints.begin() ;music_fingerprints_iterator != fingerprints.end() ; music_fingerprints_iterator++) {
        
        string fingerprint = *music_fingerprints_iterator;
        //assert( "The fingerprint given already exists in DB! Check if it's a bug or a duplicate insertion." && _fingerprintTopAudioProp.find(fingerprint) == _fingerprintTopAudioProp.end() );
        if(_fingerprintTopAudioProp.find(fingerprint) != _fingerprintTopAudioProp.end())
            cout << "The fingerprint given already exists in DB! Check if it's a bug or a duplicate insertion. Song A = " << (_fingerprintTopAudioProp.find(fingerprint)->second)->getSongName() + " by " + (_fingerprintTopAudioProp.find(fingerprint)->second)->getArtist() << " ; and Song B : "+ pAudioProperties->getSongName() + " by " + pAudioProperties->getArtist() << " ; and fingerprint : " + fingerprint   <<endl;
        _fingerprintTopAudioProp.insert(std::pair<string, AudioProperties*>(fingerprint,pAudioProperties));

    }
    
    cout << "DatabaseService >> insertSongInDatabase : New song added to database! "<< endl;
    assert("There was a different number of additions/removals of the database than excepted" && _mapMusicByID.size()- previous_number_of_songs_database == 1  );
    
    assert("Addition  to the mapMusicByID wasn't successful" && _mapMusicByID.find(pAudioProperties->getMusic()->getID()) != _mapMusicByID.end());
    assert("getMusic != Null " && pAudioProperties->getMusic()->getID() != "");
    return true; 
}






bool DatabaseService::connectToDatabase(){
    
    //_mapMusicByID = map<string,AudioProperties*>();
    //_fingerprintTopAudioProp = map<string,AudioProperties*>();
    return true;
    /*
    bool success = false;
    
    try {

     driver =sql::mysql::get_driver_instance();
     connection = driver->connect("tcp://127.0.0.1:3306/", "root", "");
        cout << " DatabaseService >> Connected to the DB " << endl;

    }
    catch(sql::SQLException &e){
        
        cout << " DatabaseService >> MySQL error code: " << e.getErrorCode() << endl;
        return success;
    
    }
    
    
    success = true;
    
    return success;
*/
}

bool DatabaseService::existsInDatabase(const string _strSongName, const string _strArtistName, const string _strAlbumName){
    
    string id =_strSongName+_strArtistName+_strAlbumName;
    return _mapMusicByID.find(id) != _mapMusicByID.end();
    

}


bool DatabaseService::saveDatabase(){

    //open destination file
    ofstream destination_file(DEFAULT_DB);
    if (destination_file.rdstate() == std::basic_ios<char>::failbit){
    
        cout << "DatabaseService >> saveDatabase : The file where the database was to be written can't be created or accessed. " << endl;
        return false;
    }
    
    destination_file << _fingerprintTopAudioProp.size() << endl;
    destination_file << _mapMusicByID.size() << endl;
    //write per line the information of one song
    
    
    
    for(map<string,AudioProperties*>::iterator musicIterator = _mapMusicByID.begin(); musicIterator != _mapMusicByID.end(); ++musicIterator) {
        AudioProperties* current_song = musicIterator->second;
        current_song->getMusic()->getID();
        
        destination_file << current_song->getHopSize()<< " " << current_song->getFrameNumber()<< " " <<current_song->getMusic()->getID() << " " << current_song->getSongName() << " "<< current_song->getAlbum() << " "<< current_song->getArtist() << " "<< current_song->getYear() << " "<< current_song->getFilePath() << endl;
        //Music(long id, string name, string album, string artist, int year, string path);

    }
    //write per line the information of fingerprint vs song
    for(map<string,AudioProperties*>::iterator songByFingerprint = _fingerprintTopAudioProp.begin(); songByFingerprint != _fingerprintTopAudioProp.end(); ++songByFingerprint) {

        destination_file << songByFingerprint->first << " " << (songByFingerprint->second)->getMusic()->getID() << endl;
        
    }
    
    return true;

}

bool DatabaseService::loadDatabase(){
    
    ifstream database_file(DEFAULT_DB);
    
    if(!database_file.good())
        return false;

    int line_size = 380;
    char line[line_size];
    database_file.getline(line, line_size);
    int databaseSize = atoi(line);
    database_file.getline(line, line_size);
    int number_of_songs = atoi(line);
    //creates songs
    for(int iteration = 0 ; iteration < number_of_songs; iteration ++){
        database_file.getline(line, line_size); //saves the line in buffer
        //cout << "line ="<< line<<endl;
        stringstream ss;
        ss << line;
        char* miniBuffer = new char[10];
        
        ss.getline( miniBuffer, 10, ' ' );
        int hopSize = atoi(miniBuffer);
        
        miniBuffer = new char[15];
        ss.getline( miniBuffer, 15, ' ' );
        int frameNumber = atoi(miniBuffer);
        
        miniBuffer = new char[300];
        ss.getline( miniBuffer, 300, ' ' );
        string id = miniBuffer;
        
        miniBuffer = new char[70];
        ss.getline( miniBuffer, 70, ' ' );
        string song_name = miniBuffer;
        
        miniBuffer = new char[100];
        ss.getline( miniBuffer, 100, ' ' );
        string album_name = miniBuffer;
        
        miniBuffer = new char[70];
        ss.getline( miniBuffer, 70, ' ' );
        string artist = miniBuffer;
        
        miniBuffer = new char[50];
        ss.getline( miniBuffer, 50, ' ' );
        int year = atoi(miniBuffer);
        miniBuffer = new char[300];
        ss.getline( miniBuffer, 300, ' ' );
        string path = miniBuffer;
        
        Music* pCurrentMusic= new Music(id, song_name, album_name, artist, year, path);
        AudioProperties *pCurrentAudioProperties = new AudioProperties(pCurrentMusic);
        pCurrentAudioProperties->setFrameNumber(frameNumber);
        pCurrentAudioProperties->setHopSize(hopSize);
        //size_t old_size = _mapMusicByID.size();
        _mapMusicByID[id] = pCurrentAudioProperties;
        //assert("MusicByID didn't change it's size besides the adding of a song!" && _mapMusicByID.size() == old_size +1);
    
    }
    
    //creates the database
    
    for(int iteration = 0 ; iteration < databaseSize; iteration ++){
        database_file.getline(line, line_size); //saves the line in buffer
        stringstream ssFingerprint;

        ssFingerprint << line;
        //cout << "line =" << line << "|" << endl;
        char* miniBuffer = new char[120];
        ssFingerprint.getline( miniBuffer, 120, ' ' );
        //cout << "minibuffer =" << miniBuffer << "|" << endl;

        string fingerprint = miniBuffer;
        miniBuffer = new char[100];
        ssFingerprint.getline( miniBuffer, 100);
        string music_id = miniBuffer;

        _mapMusicByID[music_id]->insertFingerprint(fingerprint) ;
        _fingerprintTopAudioProp[fingerprint] = _mapMusicByID[music_id];
        
    }
    assert("DatabaseService >>loadDatabase :  Database is still empty..."  &&  !_mapMusicByID.empty());
    return true;
}

bool DatabaseService::loadDatabase(string backupFile){

    return true;
}

DatabaseService::~DatabaseService(){
    clearDatabase();
}

bool DatabaseService::isEmpty(){
    
    return _mapMusicByID.empty();

}

bool DatabaseService::clearDatabase(){

    _fingerprintTopAudioProp.clear();
    assert("DatabaseService>clearDatabase >> Fingerprints weren't deleted! " && _fingerprintTopAudioProp.empty());
    _mapMusicByID.clear();
    assert("DatabaseService>clearDatabase >> Musics weren't deleted! " && _mapMusicByID.empty());

    return true;
    
}

size_t DatabaseService::getNumberOfFingerprints(){

    return _fingerprintTopAudioProp.size();

}
size_t DatabaseService::getNumberOfSongs(){

    return _mapMusicByID.size();
}




