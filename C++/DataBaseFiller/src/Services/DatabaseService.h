//
//  DatabaseService.h
//  DataBaseFiller
//
//  Created by Ana on 13/05/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#ifndef __DataBaseFiller__DatabaseService__
#define __DataBaseFiller__DatabaseService__

#include <stdio.h>
#include "../DataTypes/AudioProperties.h"
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h> 

//MySQL includes
/*
#include <mysql.h>
#include "mysql_driver.h"
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
*/
using namespace std;

class DatabaseService{
    
    public:
        ~DatabaseService();
        bool connectToDatabase();
        bool insertSongInDatabase(AudioProperties *pAudioProperties);
        bool saveDatabase();
        bool loadDatabase();
        bool loadDatabase(string backupFile);
        bool clearDatabase();
        bool deleteMusicFromDatabase();
        bool existsInDatabase(string const _strSongName, string const _strArtistName, string const _strAlbumName);
        bool isEmpty();
        size_t getNumberOfSongs();
        size_t getNumberOfFingerprints();

    private:
        map<string,  AudioProperties*> _fingerprintTopAudioProp;
        map<string,  AudioProperties*> _mapMusicByID;

};




#endif /* defined(__DataBaseFiller__DatabaseService__) */
