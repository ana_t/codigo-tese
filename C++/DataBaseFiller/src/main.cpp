//
//  main.cpp
//  DataBaseFiller
//
//  Created by Ana on 13/05/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#include <stdio.h>
#include "essentia.h"
#include "taglib.h"
#include "fftw3.h"
#include <essentia/algorithmfactory.h>
#include <essentia/essentiamath.h>
#include <essentia/pool.h>
#include <essentia/scheduler/network.h>
#include <essentia/streaming/algorithms/poolstorage.h>
#include <iostream>
#include <pthread.h>
#include <thread>
#include <ctime>



//custom classes/interfaces
#include "AudioProperties.h"
#include "Analyzers/PeakFinder.h"
#include "Services/DatabaseService.h"

#define MUSIC_INPUT 2


using namespace std;
using namespace essentia::streaming;
using namespace essentia::scheduler;


/////// PARAMS //////////////

int frameSize = 512;
int hopSize = 256;
int number_of_peaks_per_frame = 15;
int min_Frequency = 80;
int max_Frequency = 10000;
essentia::Real sampleRate = 44100.0 ;

DatabaseService database;



/*
 *  Checks if two strings are equal. In this case, compares if a command matches another
 *
 */
bool isCommand (string const attempt, string const command){

    return attempt.compare(command) == 0;

}


void manual(){
    
    cout << "\t********************************* MAN ***********************************"<< endl ;
    cout << "\tThe accepted commands for this module are the following:\n "<< endl ;
    cout << "\t\thelp (of course): shows a manual for this module, including the accepted commands, their functions, and how to use them.\n"<< endl ;
    cout << "\t\tadd <path to song> : Adds the song of the path given to the database. By using this command one also creates the unique ids of songs, to allow their identification through samples\n WARNING: the song must respect the following format: <Song Name>-<Artist>-<Album>-<Year>.wav or else IT WON'T WORK \n  Also, there can be no spaces in path!"<< endl ;
    cout << "\t\tadd <path to song> : Adds the song of the path given to the database. By using this command one also creates the unique ids of songs, to allow their identification through samples\n"<< endl ;
    cout << "\t\tdelete <path to song> (in the future) : Deletes the song of the path given to the database. By using this command one also deletes the unique ids of songs, making impossible the identification of the deleted song through samples\n"<< endl ;
    
    cout << "\t\tidentify <path to sample> (in the future) : Tries to identify the music present in the recording given \n"<< endl ;
    cout << "\t\tstatus : shows the database numbers, such as number of songs \n"<< endl ;

    cout << "\t\tdeleteDB : deletes the database contents \n"<< endl ;

    cout << "\t******************************* MAN END *********************************"<< endl ;
    cout << "Please write your command."<< endl ;

}

void getParametersForAudioProperties(string filepath , string &_strSongName, string &_strArtistName, string &_strAlbumName, int &_iAlbumYear){
    
    string original_file_path = filepath;
    size_t aux_position_of_slash =  filepath.find("/");
    
    string aux_file_name = filepath;
    while(aux_position_of_slash != std::string::npos){
        aux_file_name = aux_file_name.substr(aux_position_of_slash+1);
        aux_position_of_slash =  aux_file_name.find("/");
        
    }
    string aux = aux_file_name;
    //defines every other property of audio based on the format of the file name. - song information
    size_t aux_position_of_hifen =  aux.find("-");
    _strSongName = aux.substr(0, aux_position_of_hifen);
    aux = aux.substr( aux_position_of_hifen+1);
    aux_position_of_hifen =  aux.find("-");
    _strArtistName = aux.substr(0,aux_position_of_hifen);
    
    aux = aux.substr( aux_position_of_hifen+1);
    aux_position_of_hifen =  aux.find("-");
    _strAlbumName = aux.substr(0, aux_position_of_hifen);
    
    aux = aux.substr( aux_position_of_hifen+1);
    aux_position_of_hifen =  aux.find(".");
    string yearString = aux.substr(0, aux_position_of_hifen);
    _iAlbumYear = stoi(yearString);
}


bool addSong(){

    
    string song_path = "";
    cin >> song_path;

    string _strSongName,_strArtistName, _strAlbumName;
    int _iAlbumYear;
    getParametersForAudioProperties(song_path , _strSongName, _strArtistName, _strAlbumName, _iAlbumYear);
    
    if(database.existsInDatabase( _strSongName, _strArtistName, _strAlbumName))
        return false;
    
    AudioProperties *audio = new AudioProperties(song_path , _strSongName, _strArtistName, _strAlbumName, _iAlbumYear);
    audio->setHopSize(hopSize);
    
    
    // To register the algorithms in the factory (ies)
    essentia::init();
    
    
    //calculating the peaks for each file - *TODO* this part will be running in multithreading to improve results in case of two files
    cout << "--- Feature Extraction and its storage " << endl;
    time_t start_time = time(NULL);
    
    Pool pool = PeakFinder::find(audio, frameSize, hopSize, sampleRate,  max_Frequency,  min_Frequency,  number_of_peaks_per_frame);
    std::this_thread::sleep_for(std::chrono::seconds(3));
    audio->setFrameNumber((int)pool.value<vector<vector<essentia::Real>>>("SpectralPeaksFrequencies").size());
    audio->savePeaksFromPoolInVector(pool);
    
    //database.insertSongInDatabase(audio);
    cout << "--- Time of feature extraction (elapsed) : " << time(NULL) - start_time <<" seconds ------\n" <<endl;
    
    essentia::shutdown();
    //delete audio;

    bool success =  database.insertSongInDatabase(audio);
    delete audio;
    return success;
}

string identifySong(){

    return "\tNo song was matched against your sample.";

}


/*
 * Tries to do the strFull command. It has to search for a match and then does the corresponding command.
 
 *
 */
void doCommand(string strCommand){
    
    if (isCommand(strCommand, "help"))
        manual();
    
    else if (isCommand(strCommand, "add")){
        if(!addSong())
            cout << "add > ERROR: The song you requested can not be added because already exists in the database." << endl;
    }
    else if (isCommand(strCommand, "status")){
            cout << "\t ********* Database Status *********" << endl;
            cout << "\t Number of songs = "<< database.getNumberOfSongs() << endl;
            cout << "\t Number of fingerprints = "<< database.getNumberOfFingerprints() << "\n"<< endl;
    }
        else if (isCommand(strCommand, "deleteDB")){
            clock_t start = clock();
            if(!database.clearDatabase())
                cout << "deleteDB > ERROR: The database couldn't be cleared." << endl;
            else cout << "Database cleared successfully. It took " << (clock() - start) / CLOCKS_PER_SEC << "seconds.\n" <<endl;
        }
    
    else if (isCommand(strCommand, "quit")){
        cout << "Exiting.... Making some backups first... " << endl;
        clock_t start = clock();
        if(database.saveDatabase())
            cout << "Database saved. It took " << (clock() - start) / CLOCKS_PER_SEC << "seconds" <<endl;
        else
            cout << "There was a problem saving the database... Exiting without backup and regrets."<<endl ;
        cout << " Bye!" << endl;
        database.clearDatabase();
        exit(0);
    }
    
    else if (isCommand(strCommand, "identify")){
        cout << identifySong() <<endl;

    
    }
    else cout <<"\nCommand not recognized: " << strCommand <<endl;
    
    
    
    
}




/**************************************************************************************************************************
 ***************************************************************************************************************************
 ***********************************************         MAIN        *******************************************************
 ***************************************************************************************************************************
 ***************************************************************************************************************************/




int main(int argc, const char * argv[]) {
    
    cout << "Welcome to Database Filler!" << endl;
    cout << "\n >>> WARNING :  For every song add/delete the song must respect the following format: <Song Name>-<Artist>-<Album>-<Year>.wav or else IT WON'T WORK.\n Also, there can be no spaces in path!\n"<<endl;
    
    assert ("ERROR: couldn't connect to database" && database.connectToDatabase() );
    cout << "Loading database..." << endl;
    clock_t start = clock();
    if(!database.loadDatabase())
        cout << "Warning: No database detected or file is corrupted. The program will continue and make a new database from now on."<<endl;
    else
        cout << "Done. It took " << (clock() - start) / CLOCKS_PER_SEC << "seconds" <<endl;
    //assert("Main >> Database is empty after load!" && !database.isEmpty());
    bool isRunning = true;
    string strFullCommand = "";
    cout << "Please write your command. If you need to get the manual, please type 'help' + enter. " << endl;

    while (isRunning) {
        string strCommand;
        //waits for command
        cin >> strCommand;
        doCommand(strCommand);
        cout << "Insert new command: " << endl;
    }
     /*
        string connect  = (DatabaseService::connectToDatabase()) ? "YES!":"NO";
    cout <<" Can I connect to the database??? A: " << connect << endl;
    */
}