//
//  Music.cpp
//  DataBaseFiller
//
//  Created by Ana on 25/05/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#include "Music.h"

Music::Music(string id, string name, string album, string artist, int year, string path){
    _strAlbumName   = album;
    _strArtistName  = artist;
    _strFilePath    = path;
    _strSongName    = name;
    _iAlbumYear     = year;
    _iID            = name + artist + album ;

}

string Music::getSongName(){
    return _strSongName;

}
string Music::getFilePath(){
    return _strFilePath;
}
string Music::getAlbum(){
    return _strAlbumName;
}
string Music::getArtist(){
    return _strArtistName;
}
int Music::getYear(){
    return _iAlbumYear;
}
/*
void Music::setID(long id){

    _iID = id;

}
*/
string Music::getID(){
    return _iID;
}

Music::~Music(){
    
}
