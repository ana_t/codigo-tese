//
//  Peak.h
//  Spectrogram
//
//  Created by Ana on 04/05/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#ifndef __Spectrogram__Peak__
#define __Spectrogram__Peak__

#include <stdio.h>
#include "essentia.h"

#endif /* defined(__Spectrogram__Peak__) */

using namespace essentia;
class Peak{

    public:
        Peak(essentia::Real freq, essentia::Real mag, essentia::Real t, int f);
        Peak(const Peak &peak);
        essentia::Real* getFrequency();
        essentia::Real* getMagnitude();
        essentia::Real* getTime();
        int * getFrame();
        bool equals(Peak* p);
        std::string toString();
        ~Peak();
    private:
        essentia::Real * frequency;
        essentia::Real * magnitude;
        essentia::Real * time;
        int* frame; //in the future will be erased

};