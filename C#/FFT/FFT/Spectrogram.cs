﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FFT
{
    public partial class Spectrogram : UserControl
    {
        public Spectrogram()
        {
            InitializeComponent();
        }
        /*
         *Atribui valores as colunas do espectrograma, atraves de uma lista
         */
        public void Set(List<int> data)
        {

            if (data.Count < 16) return;
            Console.WriteLine("Valores buffer: ");
            progressBar1.Value = data[0];
            Console.WriteLine(data[1] + " ");
            Console.WriteLine(data[2] + " ");
            Console.WriteLine(data[3] + " ");
            Console.WriteLine(data[4] + " ");
            Console.WriteLine(data[5] + " ");
            Console.WriteLine(data[6] + " ");
            Console.WriteLine(data[7] + " ");
            Console.WriteLine(data[8] + " ");
            Console.WriteLine(data[9] + " ");
            Console.WriteLine(data[10] + " ");
            Console.WriteLine(data[11] + " ");

            progressBar1.Refresh();
            System.Threading.Thread.Sleep(5000);
 
            progressBar2.Value = data[1];
            progressBar3.Value = data[2];
            progressBar4.Value = data[3];
            progressBar5.Value = data[4];
            progressBar6.Value = data[5];
            progressBar7.Value = data[6];
            progressBar8.Value = data[7];
            progressBar9.Value = data[8];
            progressBar10.Value = data[9];
            progressBar11.Value = data[10];
            progressBar12.Value = data[11];
            progressBar13.Value = data[12];
            progressBar14.Value = data[13];
            progressBar15.Value = data[14];
            progressBar16.Value = data[15];
        }




    }
}
