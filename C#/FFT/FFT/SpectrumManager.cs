﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Un4seen.Bass;
using Un4seen.BassWasapi;

namespace FFT
{
    class SpectrumManager{
    private const int BUFFER_SIZE = 1024;
    private const int PROGRESS_BARS_NUMBER = 16;

    private bool _bFileSelected;
    private bool _bRunning;               //enabled status
    private DispatcherTimer _DTtimer;   //timer that refreshes the display
    private float[] _faFFT;               //buffer for fft data
    private List<int> _lbSpectrumdata;   //spectrum data buffer
    private Spectrogram _spectrogram;         //spectrum dispay control
    private String _sFilename;              // path do ficheiro a ler
    private String _sErrorMessage;
    private int _iStream;
    private bool _isPlaying;
        
        public SpectrumManager(Spectrogram spectrogram)
    {
        _bRunning = false;
        _spectrogram = new Spectrogram();
        _faFFT = new float[BUFFER_SIZE];
        _DTtimer = new DispatcherTimer();
        //_DTtimer.Tick += _t_Tick;
        _DTtimer.Interval = TimeSpan.FromMilliseconds(25); //40hz refresh rate
        _DTtimer.IsEnabled = false;
        _DTtimer.Tick += soundInfoUpdater;
        _sFilename = null;
        _sErrorMessage = null;
        _iStream = 0;
        _isPlaying = false;
        _lbSpectrumdata = new List<int>();

        // Init Bass
        Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);


    }
        public String errorMessage
        {
            get
            {
                return _sErrorMessage;
            }
            set
            {
                _sErrorMessage = value;
            }
        }
        public int stream
        {
            set
            {
                if(_iStream !=0)
                    Bass.BASS_StreamFree(_iStream);
                _iStream = value;
            }
            get
            {
                return _iStream;
            }

        }

        
        public bool running
        {
            set { _bRunning = value;
            Console.WriteLine("Set do running\n");

            // create the stream 
            if (_iStream == 0)
            {
                _iStream = Bass.BASS_StreamCreateFile(_sFilename, 0, 0,
                                  BASSFlag.BASS_SAMPLE_FLOAT | BASSFlag.BASS_STREAM_PRESCAN);
                _isPlaying = false;
                Console.WriteLine("Novo stream\n");


            }
             if (_iStream != 0 && !_isPlaying)
            {              
                // playing
                Bass.BASS_ChannelPlay(_iStream, false);
                _sErrorMessage += "Playing... \n";
                Console.WriteLine("PLAY stream\n");
                _isPlaying = true;
                _DTtimer.IsEnabled = value;


            }
            else if(_iStream != 0 && _isPlaying){
                Bass.BASS_ChannelPause(_iStream);
                _sErrorMessage += "Paused... \n";
                _isPlaying = false;
                Console.WriteLine("PAUSE stream\n");


            }
            else if (Bass.BASS_ErrorGetCode() != BASSError.BASS_OK)
            {
                _sErrorMessage += "Error on reading file. Please try a different one.";
                Console.WriteLine("Error={0}", Bass.BASS_ErrorGetCode());
            }
       
            }
            get { return _bRunning;  }
        }
        public bool selectFile
        {

            set { _bFileSelected = value; }
            get { return _bFileSelected; }
        }

        public String file
        {

            set { _sFilename = value; }
            get { return _sFilename; }
        }

        private void soundInfoUpdater(object sender, EventArgs e)
        {
            int bytes_read = Bass.BASS_ChannelGetData(_iStream, _faFFT, BUFFER_SIZE);
            if (bytes_read != -1)
            {

                int x, y;
                int b0 = 0;

                //computes the spectrum data, the code is taken from a bass_wasapi sample.
                for (x = 0; x < PROGRESS_BARS_NUMBER; x++)
                {
                    //para cada barra de progresso, comeca-se com peak = 0
                    float peak = 0;
                    int b1 = (int)Math.Pow(2, x * 10.0 / (PROGRESS_BARS_NUMBER - 1));
                    if (b1 > 1023) b1 = 1023;
                    if (b1 <= b0) b1 = b0 + 1;
                    //para cada unidade de diferenca entre b0 e b1, 
                    for (; b0 < b1; b0++)
                    {
                        //se peak < valor no buffer [1 +b0]
                        if (peak < _faFFT[1 + b0]) peak = _faFFT[1 + b0];
                    }
                    y = (int)(Math.Sqrt(peak) * 3 * 255 - 4);
                    if (y > 255) y = 255;
                    if (y < 0) y = 0;
                    _lbSpectrumdata.Add(y);
                    //Console.Write("{0, 3} ", y);
                }
                _spectrogram.Set(_lbSpectrumdata);
            }

        }





    }

        
}
